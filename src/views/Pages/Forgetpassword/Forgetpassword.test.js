import React from 'react';
import ReactDOM from 'react-dom';
import Forgetpassword from './Forgetpassword';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Forgetpassword />, div);
  ReactDOM.unmountComponentAtNode(div);
});

import React, { Component } from 'react';
import { 
  Card, CardHeader, CardBody, CardFooter,
  Badge, Collapse,
  Row, Col, 
  Pagination, PaginationItem, PaginationLink, 
  Table, 
  Nav, NavItem, NavLink, TabContent, TabPane, 
  Button, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Form, FormGroup, FormText, FormFeedback, Input, InputGroup, InputGroupAddon, InputGroupButtonDropdown, InputGroupText, Label 
} from 'reactstrap';
import classnames from 'classnames';
import DataTable, { memoize } from 'react-data-table-component';
import axios, { post } from 'axios';

const columns = [
  {
    name: 'No. AJU',
    selector: 'aju',
    sortable: false,
  },
  {
    name: 'Plate Number',
    selector: 'plate',
    sortable: false,
  },
  {
    name: 'Name of Owner',
    selector: 'owner',
    sortable: false,
  },
  {
    name: 'Name of Driver',
    selector: 'driver',
    sortable: false,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: false,
  }
];

class Import extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleDetailTabs = this.toggleDetailTabs.bind(this);
    this.toggleDetail = this.toggleDetail.bind(this);
    this.toggleDropdownMenu = this.toggleDropdownMenu.bind(this);
    this.toggleOwnerInformation = this.toggleOwnerInformation.bind(this);
    this.toggleDriverInformation = this.toggleDriverInformation.bind(this);
    this.toggleVechileInformation = this.toggleVechileInformation.bind(this);
    this.toggleFormOwner = this.toggleFormOwner.bind(this);
    this.toggleFormDriver = this.toggleFormDriver.bind(this);
    this.toggleFormVechile = this.toggleFormVechile.bind(this);
    this.submitVhd = this.submitVhd.bind(this);
    this.saveVhd = this.saveVhd.bind(this);
    this.saveOwner = this.saveOwner.bind(this);
    this.saveDriver = this.saveDriver.bind(this);
    this.saveVechile = this.saveVechile.bind(this);
    this.state = {
      dropdownMenuOpen: new Array(1).fill(false),
      modalDetailTitle: 'VhD Detail',
      modalDetailContent: '',
      modalDetailButton: '',
      modalDetail: false,
      modalFooter: true,
      subModal: false,
      subModalContent: '',
      subModalButton: '',
      activeTab: new Array(6).fill('1'),
      activeDetailTab: new Array(4).fill('1'),
      collapseOwnerInformation: false,
      collapseDriverInformation: false,
      collapseVechileInformation: false,
      submitVhd: false,
      details: false,
      country_options: [
        {
          id: 1,
          name: 'Malaysia'
        },
        {
          id: 2,
          name: 'Brunai Darussalam'
        },
        {
          id: 3,
          name: 'Timor Leste'
        },
        {
          id: 4,
          name: 'Papua Nugini'
        }
      ],
      default_plbn_options: [
        {
          country: 1,
          id: 1,
          name: 'PPLB Aruk (Kalbar)'
        },
        {
          country: 1,
          id: 2,
          name: 'PPLB Entikong (Kalbar)'
        },
        {
          country: 1,
          id: 3,
          name: 'PPLB Nanga Badau (Kalbar)'
        }
      ],
      plbn_options: [],
      plbn_data: [
        {
          country: 1,
          id: 1,
          name: 'PPLB Aruk (Kalbar)'
        },
        {
          country: 1,
          id: 2,
          name: 'PPLB Entikong (Kalbar)'
        },
        {
          country: 1,
          id: 3,
          name: 'PPLB Nanga Badau (Kalbar)'
        },
        {
          country: 2,
          id: 4,
          name: 'PPLB Aruk (Kalbar)'
        },
        {
          country: 2,
          id: 5,
          name: 'PPLB Entikong (Kalbar)'
        },
        {
          country: 3,
          id: 6,
          name: 'PPLB Nanga Badau (Kalbar)'
        },
        {
          country: 3,
          id: 7,
          name: 'PPLB Wini (NTT)'
        },
        {
          country: 3,
          id: 8,
          name: 'PPLB Motaain (NTT)'
        },
        {
          country: 3,
          id: 9,
          name: 'PPLB Motamasin (NTT)'
        },
        {
          country: 4,
          id: 10,
          name: 'PPLB Skouw (Papua)'
        }
      ],
      identityTypes: [
        {
          id: 1,
          name: 'Passport'
        },
        {
          id: 2,
          name: 'Driver License'
        }
      ],
      form_error_message: '',
      label_owner_name: '',
      form_owner_title: 'Mr.',
      form_owner_name: '',
      form_owner_address: '',
      form_owner_identity: '',
      form_owner_identityNumber: '',
      form_owner_identity_doc: '',
      form_owner_photo: '',
      form_owner_photo_path: '',
      label_driver_name: '',
      form_driver_title: 'Mr.',
      form_driver_name: '',
      form_driver_address: '',
      form_driver_driverLicense: '',
      form_driver_license_doc: '',
      form_driver_photo: '',
      form_driver_photo_path: '',
      label_vechile_plate: '',
      form_vechile_cardNumber: '',
      form_vechile_plate: '',
      form_vechile_country: '',
      form_vechile_brand: '',
      form_vechile_model: '',
      form_vechile_chassisNumber: '',
      form_vechile_engineNumber: '',
      form_vechile_yearManufactured: '',
      form_vechile_color: '',
      form_vechile_destinationAddress: '',
      form_vhd_provinceDestination: '',
      form_vhd_enter_from: '1',
      form_vhd_plbn_in: '1',
      form_vhd_entrance_date: '',
      form_multitrip_search_message: '',
      form_multitrip_number: 0,
      form_multitrip_vhd_reg_number: '',
      form_multitrip_vhd_id: '',
      form_multitrip_exit_from: '',
      form_multitrip_plbn_out: '',
      form_multitrip_exit_date: '',
      form_extension_search_message: '',
      form_extension_vhd_reg_number: '',
      form_extension_vhd_id: '',
      form_extension_number: 0,
      form_extension_quota: 150,
      form_extension_checked_request: false,
      form_accomplishment_search_message: '',
      form_accomplishment_vhd_reg_number: '',
      form_accomplishment_vhd_id: '',
      form_accomplishment_type: 'Export Back',
      form_accomplishment_depart_to: '',
      form_accomplishment_gate_out: '',
      form_accomplishment_exit_date: '',
      form_accomplishment_cause: '',
      form_accomplishment_vechile_owner_approval: '',
      form_accomplishment_bc_statement: '',
      form_accomplishment_bnpb_statement: '',
      form_accomplishment_police_statement: '',
      perPage: 10,
      loading_itemsAll: false, total_itemsAll: 0,
      itemsAll: [],
      itemsInProcess: [],
      loading_itemsInProcess: false, total_itemsInProcess: 0,
      itemsOngoing: [],
      loading_itemsOngoing: false, total_itemsOngoing: 0,
      itemsDone: [],
      loading_itemsDone: false, total_itemsDone: 0,
      itemsDraft: [],
      loading_itemsDraft: false, total_itemsDraft: 0,
      itemsRejected: [],
      loading_itemsRejected: false, total_itemsRejected: 0,
    };
  }
  fetchingDataVhD(url, state_name, per_page, page) {
    this.setState({ ['loading_'+state_name]: true });
    fetch(`${url}?page=${page}&per_page=${per_page}`)
      .then(res => res.json())
      .then(parsedJSON => this.setState({
        [state_name]: parsedJSON.results,
        ['total_'+state_name]: parsedJSON.results.length,
        ['loading_'+state_name]: false,
      }))
      .catch(error => console.log(`parsing data from ${url} failed`, error));
  }
  async componentDidMount() {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/all.json', 'itemsAll', perPage, 1);
    this.fetchingDataVhD('/data/import/inprocess.json', 'itemsInProcess', perPage, 1);
    this.fetchingDataVhD('/data/import/ongoing.json', 'itemsOngoing', perPage, 1);
    this.fetchingDataVhD('/data/import/done.json', 'itemsDone', perPage, 1);
    this.fetchingDataVhD('/data/import/draft.json', 'itemsDraft', perPage, 1);
    this.fetchingDataVhD('/data/import/rejected.json', 'itemsRejected', perPage, 1);
    this.setState({plbn_options: this.state.default_plbn_options});
  }

  handlePageItemsAllChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/all.json', 'itemsAll', perPage, page);
  }
  handlePerRowsItemsAllChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/all.json', 'itemsAll', perPage, page);
  }
  
  handlePageItemsInProcessChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/inprocess.json', 'itemsInProcess', perPage, page);
  }
  handlePerRowsItemsInProcessChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/inprocess.json', 'itemsInProcess', perPage, page);
  }

  handlePageItemsOngoingChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/ongoing.json', 'itemsOngoing', perPage, page);
  }
  handlePerRowsItemsOngoingChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/ongoing.json', 'itemsOngoing', perPage, page);
  }

  handlePageItemsDoneChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/done.json', 'itemsDone', perPage, page);
  }
  handlePerRowsItemsDoneChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/done.json', 'itemsDone', perPage, page);
  }

  handlePageItemsDraftChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/draft.json', 'itemsDraft', perPage, page);
  }
  handlePerRowsItemsDraftChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/draft.json', 'itemsDraft', perPage, page);
  }

  handlePageItemsRejectedChange = async (page) => {
    const { perPage } = this.state;
    this.fetchingDataVhD('/data/import/rejected.json', 'itemsRejected', perPage, page);
  }
  handlePerRowsItemsRejectedChange = async (perPage, page) => {
    this.fetchingDataVhD('/data/import/rejected.json', 'itemsRejected', perPage, page);
  }

  toggleDetail() {
    this.setState({
      modalDetail: !this.state.modalDetail
    }, () => {
      if ( !this.state.modalDetail ) {
        this.setState({
          collapseOwnerInformation: false,
          collapseDriverInformation: false,
          collapseVechileInformation: false
        });
      }
    });
  }
  toggleSubModal() {
    this.setState({
      subModal: !this.state.subModal
    }, () => { console.log(this.state.subModal)});
  }
  formVhdHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    if ( nam === 'form_owner_photo' || nam === 'form_driver_photo' ) {
      var reader = new FileReader();
      reader.onload = () => {
        if ( nam === 'form_owner_photo' ) {
          this.setState({form_owner_photo_path: reader.result}, () => {
            this.formVhD(false);
          });
        } else {
          this.setState({form_driver_photo_path: reader.result}, () => {
            this.formVhD(false);
          });
        }
      };
      reader.readAsDataURL(event.target.files[0]);
      this.setState({[nam]: event.target.files[0]});
    } else if ( nam === 'form_owner_identity_doc' || nam === 'form_driver_license_doc' ) {
      this.setState({[nam]: event.target.files[0]});
    } else {
      this.setState({
        [nam]: val
      }, () => {
        if ( nam === 'form_vhd_enter_from' ) {
          let options = [];
          for(var i = 0; i < this.state.plbn_data.length; i++) {
            let item = this.state.plbn_data[i];
            console.log(item.country + ': '+ val);
            if ( parseInt(item.country) === parseInt(val) ) {
              console.log(item.country + ': '+ val);
              options.push(item);
            }
          }
          this.setState({plbn_options: options}, () => {
            this.formVhD(false);
          });
        }
      });
    }
  }
  resetFormVhd() {
    this.setState({
      form_error_message: '',
      label_owner_name: '',
      form_owner_title: 'Mr.',
      form_owner_name: '',
      form_owner_address: '',
      form_owner_identity: '',
      form_owner_identityNumber: '',
      form_owner_identity_doc: '',
      form_owner_photo: '',
      form_owner_photo_path: '',
      label_driver_name: '',
      form_driver_title: 'Mr.',
      form_driver_name: '',
      form_driver_address: '',
      form_driver_driverLicense: '',
      form_driver_license_doc: '',
      form_driver_photo: '',
      form_driver_photo_path: '',
      label_vechile_plate: '',
      form_vechile_cardNumber: '',
      form_vechile_plate: '',
      form_vechile_country: '',
      form_vechile_brand: '',
      form_vechile_model: '',
      form_vechile_chassisNumber: '',
      form_vechile_engineNumber: '',
      form_vechile_yearManufactured: '',
      form_vechile_color: '',
      form_vechile_destinationAddress: '',
      form_vhd_provinceDestination: '',
      form_vhd_enter_from: '1',
      form_vhd_plbn_in: '1',
      form_vhd_entrance_date: '',
      plbn_options: this.state.default_plbn_options,
      submitVhd: false
    });
  }
  toggleOwnerInformation = (index, list) => {
    this.setState({ collapseOwnerInformation: !this.state.collapseOwnerInformation }, () => {
      this.viewDetail(index, list, false);
    });
  }
  toggleDriverInformation = (index, list) => {
    this.setState({ collapseDriverInformation: !this.state.collapseDriverInformation }, () => {
      this.viewDetail(index, list, false);
    });
  }
  toggleVechileInformation = (index, list) => {
    this.setState({ collapseVechileInformation: !this.state.collapseVechileInformation }, () => {
      this.formVhD(index, list, false);
    });
  }
  toggleFormOwner = () => {
    this.setState({ collapseOwnerInformation: !this.state.collapseOwnerInformation }, () => {
      this.formVhD(false);
    });
  }
  toggleFormDriver = () => {
    this.setState({ collapseDriverInformation: !this.state.collapseDriverInformation }, () => {
      this.formVhD(false);
    });
  }
  toggleFormVechile = () => {
    this.setState({ collapseVechileInformation: !this.state.collapseVechileInformation }, () => {
      this.formVhD(false);
    });
  }
  toggleFormMultitripOwner = () => {
    this.setState({ collapseOwnerInformation: !this.state.collapseOwnerInformation }, () => {
      this.formMultitrip(false);
    });
  }
  toggleFormMultitripDriver = () => {
    this.setState({ collapseDriverInformation: !this.state.collapseDriverInformation }, () => {
      this.formMultitrip(false);
    });
  }
  toggleFormMultitripVechile = () => {
    this.setState({ collapseVechileInformation: !this.state.collapseVechileInformation }, () => {
      this.formMultitrip(false);
    });
  }
  toggleFormAccomplishmentOwner = () => {
    this.setState({ collapseOwnerInformation: !this.state.collapseOwnerInformation }, () => {
      this.formAccomplishment(false);
    });
  }
  toggleFormAccomplishmentDriver = () => {
    this.setState({ collapseDriverInformation: !this.state.collapseDriverInformation }, () => {
      this.formAccomplishment(false);
    });
  }
  toggleFormAccomplishmentVechile = () => {
    this.setState({ collapseVechileInformation: !this.state.collapseVechileInformation }, () => {
      this.formAccomplishment(false);
    });
  }
  saveOwner(e) {
    e.preventDefault();
    const owner_full_name = this.state.form_owner_title + ' ' + this.state.form_owner_name;
    this.setState({collapseOwnerInformation: false, label_owner_name: owner_full_name}, () => {
      this.formVhD(false);
    });
    return false;
  }
  saveDriver(e) {
    e.preventDefault();
    const driver_full_name = this.state.form_driver_title + ' ' + this.state.form_driver_name;
    this.setState({collapseDriverInformation: false, label_driver_name: driver_full_name}, () => {
      this.formVhD(false);
    });
    return false;
  }
  saveVechile(e) {
    e.preventDefault();
    const { form_vechile_plate } = this.state;
    this.setState({collapseVechileInformation: false, label_vechile_plate: form_vechile_plate}, () => {
      this.formVhD(false);
    });
    return false;
  }
  saveVhd() {
    const url = '';
    const formData = new FormData();
    formData.set('owner_title', this.state.form_owner_title)
    formData.set('owner_name', this.state.form_owner_name)
    formData.set('owner_address',this.state.form_driver_address)
    formData.set('owner_identity', this.state.form_owner_identity)
    formData.set('owner_identityNumber', this.state.form_owner_identityNumber)
    formData.append('owner_identity_doc', this.state.form_owner_identity_doc)
    formData.append('owner_photo',this.state.form_owner_photo)
    formData.set('driver_title',this.state.form_driver_title)
    formData.set('driver_name',this.state.form_driver_name)
    formData.set('driver_address',this.state.form_driver_address)
    formData.set('driver_driverLicense',this.state.form_driver_driverLicense)
    formData.append('driver_license_doc',this.state.form_driver_license_doc)
    formData.append('driver_photo',this.state.form_driver_photo)
    formData.set('vechile_cardNumber',this.state.form_vechile_cardNumber)
    formData.set('vechile_plate',this.state.form_vechile_plate)
    formData.set('vechile_country',this.state.form_vechile_country)
    formData.set('vechile_brand',this.state.form_vechile_brand)
    formData.set('vechile_model', this.state.form_vechile_model)
    formData.set('vechile_choassiaNumber',this.state.form_vechile_chassisNumber)
    formData.set('vechile_engineNumber',this.state.form_vechile_engineNumber)
    formData.set('vechile_yearManufactured',this.state.form_vechile_yearManufactured)
    formData.set('vechile_color',this.state.form_vechile_color)
    formData.set('vechile_destinationAddress',this.state.form_vechile_destinationAddress)
    formData.set('vhd_enter_from',this.state.form_vhd_enter_from)
    formData.set('vhd_plbn_in',this.state.form_vhd_plbn_in)
    formData.set('vhd_entrance_date',this.state.form_vhd_entrance_date)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    this.setState({submitVhd: true}, () => {
      this.formVhD(false);
      axios({
        method: 'post',
        url: url,
        data: formData,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then((response) => {
        //handle success
        this.setState({submitVhd: false});
        this.toggleDetail();
        this.resetFormVhd();
      })
      .catch((response) => {
        //handle error
        this.setState({submitVhd: false, form_error_message: 'Error: Failed to submit data!'}, () => {
          this.formVhD(false)
        });
      });
    });
  }
  submitVhd(e) {
    e.preventDefault(); //Stop form submit
    if ( this.state.label_owner_name === '' || this.state.label_driver_name === '' || this.state.label_vechile_plate === '' ) {
      let string_messages = '';
      if ( this.state.label_owner_name === '' ) {
        string_messages += 'Please provide information of owner';
      }
      if ( this.state.label_driver_name === '' ) {
        if ( string_messages.length < 1 ) 
          string_messages += 'Please provide information of driver';
        else 
          string_messages += ', driver';
      }
      if ( this.state.label_vechile_plate === '' ) {
        if ( string_messages.length < 1 ) 
          string_messages += 'Please provide information of vechile';
        else 
          string_messages += ', and vechile';
      }
      string_messages += '.';
      this.setState({
        subModal: !this.state.subModal,
        subModalContent: string_messages,
        subModalButton: (
          <Button color="secondary" onClick={this.toggleSubModal.bind(this)}>Ok</Button>
        )
      });
      return false;
    }
    const importer_declaration = (
      <>
        <h3 className="modal-title">Importer Declaration</h3>
        <p>&nbsp;</p>
        <ol>
          <li>Bahwa saya akan menyampaikan Vechile Declaration kepada Pejabat Bea dan Cukai baik saat keluar dari daerah pabean untuk proses ekspor maupun saat masuk ke daerah pabean untuk proses impor kembali.</li>
          <li>Bahwa saya menerima penetapan nilai pabean dan tarif yang dilakukan oleh Pejabat Bea dan Cukai dalam rangka pemenuhan kewajiban kepabeanan.</li>
          <li>Bahwa saya bertanggung jawab terhadap kendaraan bermotor untuk tidak dilakukan perubahan dan sanggup membayar bea masuk dan pajak dalam rangka impor apabila terjadi terhadap bagian-bagian (parts) pengganti atau ditambahkan, serta biaya perbaikannya termasuk ongkos angkutan dan asuransi pada kendaraan bermotor.</li>
          <li>Apabila saya tidak melakukan ketentuan diatas, maka saya tidak keberatan atas tindakan Pejabat Bea dan Cukai untuk mencegah hingga melakukan penyitaan atas kendaraan dan barang yang dimuatnya yang saya kuasai untuk penyelesaian sesuai ketentuan yang berlaku.</li>
        </ol>
      </>
    );
    this.setState({
      subModal: !this.state.subModal,
      subModalContent: importer_declaration,
      subModalButton: (
        <>
        <Button color="secondary" onClick={this.toggleSubModal.bind(this)}>Cancel</Button>&nbsp;
        <Button color="secondary" onClick={() => {
          this.toggleSubModal();
          this.saveVhd();
        }}>I Aggree</Button>
        </>
      )
    });
  }
  formVhD(showModal) {
    if ( typeof showModal === 'undefined' )
      showModal = true;
    const formView = (
      <>
      <Form onSubmit={this.saveOwner}>
        {
          this.state.form_error_message === '' 
          ? null
          : (
              <div className="alert alert-danger">{this.state.form_error_message}</div>
            )
        }
        <FormGroup>
          <Label>Owner Information</Label>
          <Card>
            <CardHeader>
              {this.state.label_owner_name === '' ? '-': this.state.label_owner_name}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseOwnerInformation" onClick={() => { this.toggleFormOwner() }}>{this.state.collapseOwnerInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseOwnerInformation} id="collapseOwnerInformation">
              <CardBody>
                <FormGroup row>
                  <Col md="4">
                    <Label>Title</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input name="form_owner_title" type="select" innerRef={this.state.form_owner_title} defaultValue={this.state.form_owner_title} onChange={(ev) => this.formVhdHandler(ev)}>
                      <option value="Mr.">Mr.</option>
                      <option value="Mrs.">Mrs.</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Name</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_owner_name" placeholder="Name of Owner" innerRef={this.state.form_owner_name} defaultValue={this.state.form_owner_name} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Address</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_owner_address" placeholder="Address of Owner" innerRef={this.state.form_owner_address} defaultValue={this.state.form_owner_address} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Type Of Owner Identity</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="select" name="form_owner_identity" innerRef={this.state.form_owner_identity} defaultValue={this.state.form_owner_identity} onChange={(ev) => this.formVhdHandler(ev)}>
                      {
                        this.state.identityTypes.map((item, index) => {
                          const {id, name} = item;
                          return (
                            <option key={index} value={id}>{name}</option>   
                          );
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Number of Owner&rsquo;s Identity</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_owner_identityNumber" placeholder="Identity Number" innerRef={this.state.form_owner_identity} defaultValue={this.state.form_owner_identity} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Identity Doc</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="file" name="form_owner_identity_doc" onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Photo</Label>
                    <img src={this.state.form_owner_photo_path === '' ? '/assets/img/photo-profile.png': this.state.form_owner_photo_path} className="img-fluid"/>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="file" name="form_owner_photo" onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup className="text-center">
                  <Button type="reset" color="secondary">Reset</Button>&nbsp;
                  <Button type="submit" id="btnSubmitOwner" color="primary">Save</Button>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
      </Form>
      <Form onSubmit={this.saveDriver}>
        <FormGroup>
          <Label>Driver Information</Label>
          <Card>
            <CardHeader>
              {this.state.label_driver_name === '' ? '-': this.state.label_driver_name}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseDriverInformation" onClick={() => { this.toggleFormDriver() }}>{this.state.collapseDriverInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseDriverInformation} id="collapseDriverInformation">
              <CardBody>
                <FormGroup row>
                  <Col md="4">
                    <Label>Title</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input name="form_driver_title" type="select" innerRef={this.state.form_driver_title} defaultValue={this.state.form_driver_title} onChange={(ev) => this.formVhdHandler(ev)}>
                      <option value="Mr.">Mr.</option>
                      <option value="Mrs.">Mrs.</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Name</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_driver_name" placeholder="Name of Driver" innerRef={this.state.form_driver_name} defaultValue={this.state.form_driver_name} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Address</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_driver_address" placeholder="Address of Driver" innerRef={this.state.form_driver_address} defaultValue={this.state.form_driver_address} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Driver License</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_driver_driverLicense" placeholder="ID of Driver License" innerRef={this.state.form_driver_driverLicense} defaultValue={this.state.form_driver_driverLicense} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Driver License Doc</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="file" name="form_driver_license_doc" onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Photo</Label>
                    <img src={this.state.form_driver_photo_path === '' ? '/assets/img/photo-profile.png': this.state.form_driver_photo_path} className="img-fluid"/>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="file" name="form_driver_photo" onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup className="text-center">
                  <Button type="reset" color="secondary">Reset</Button>&nbsp;
                  <Button type="submit" id="btnSubmitDriver" color="primary">Save</Button>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
      </Form>
      <Form onSubmit={this.saveVechile}>
        <FormGroup>
          <Label>Vechile Information</Label>
          <Card>
            <CardHeader>
              {this.state.label_vechile_plate === '' ? '-': this.state.label_vechile_plate}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseVechileInformation" onClick={() => { this.toggleFormVechile() }}>{this.state.collapseVechileInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseVechileInformation} id="collapseVechileInformation">
              <CardBody>
                <FormGroup row>
                  <Col md="4">
                    <Label>Vechile Registration Card</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_cardNumber" innerRef={this.state.form_vechile_cardNumber} defaultValue={this.state.form_vechile_cardNumber} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Registration Plate Number</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_plate" innerRef={this.state.form_vechile_plate} defaultValue={this.state.form_vechile_plate} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Brand</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_brand" innerRef={this.state.form_vechile_brand} defaultValue={this.state.form_vechile_brand} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Model</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_model" innerRef={this.state.form_vechile_model} defaultValue={this.state.form_vechile_model} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Chassis Number</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_chassisNumber" innerRef={this.state.form_vechile_chassisNumber} defaultValue={this.state.form_vechile_chassisNumber} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Engine Number</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_engineNumber" innerRef={this.state.form_vechile_engineNumber} defaultValue={this.state.form_vechile_engineNumber} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Year Manufactured</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_yearManufactured" innerRef={this.state.form_vechile_yearManufactured} defaultValue={this.state.form_vechile_yearManufactured} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Colour of Vechile</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_color" innerRef={this.state.form_vechile_color} defaultValue={this.state.form_vechile_color} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Address of Destination</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="text" name="form_vechile_destinationAddress" innerRef={this.state.form_vechile_destinationAddress} defaultValue={this.state.form_vechile_destinationAddress} required onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
                <FormGroup className="text-center">
                  <Button type="reset" color="secondary">Reset</Button>&nbsp;
                  <Button type="submit" id="btnSubmitVechile" color="primary">Save</Button>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
      </Form>
      <Form onSubmit={this.submitVhd}>
        <FormGroup>
          <Label>Entrance Information</Label>
          <Card>
            <CardBody>
              <div className="form-horizontal">
                <FormGroup row>
                  <Col md="4">
                    <Label>Enter From</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="select" name="form_vhd_enter_from" innerRef={this.state.form_vhd_enter_from} defaultValue={this.state.form_vhd_enter_from} onChange={(ev) => this.formVhdHandler(ev)}>
                      {
                        this.state.country_options.map((item, index) => {
                          const {id, name} = item;
                          return (
                            <option key={index} value={id}>{name}</option>   
                          );
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>PLBN In</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="select" name="form_vhd_plbn_in" innerRef={this.state.form_vhd_plbn_in} defaultValue={this.state.form_vhd_plbn_in} onChange={(ev) => this.formVhdHandler(ev)}>
                      {
                        this.state.plbn_options.map((item, index) => {
                          const {id, name} = item;
                          return (
                            <option key={index} value={id}>{name}</option>   
                          );
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="4">
                    <Label>Entrance Date</Label>
                  </Col>
                  <Col xs="12" md="8">
                    <Input type="date" name="form_vhd_entrance_date" innerRef={this.state.form_vhd_entrance_date} defaultValue={this.state.form_vhd_entrance_date} onChange={(ev) => this.formVhdHandler(ev)} />
                  </Col>
                </FormGroup>
              </div>
            </CardBody>
          </Card>
        </FormGroup>
        <FormGroup className="text-center">
          <Button color="secondary" onClick={this.toggleDetail}>Cancel</Button>&nbsp;
          <Button type="submit" id="btnSubmit" color="primary" disabled={this.state.submitVhd}>{this.state.submitVhd ? (<><i className="fa fa-spinner fa-spin"></i> Processing</>): 'Submit'}</Button>
        </FormGroup>
      </Form>
      </>
    );
    this.setState({
      modalDetailContent: formView,
      modalDetailTitle: 'Create VhD',
      modalFooter: false
    });
    if ( showModal )
      this.toggleDetail();
  }
  resetFormMultitrip() {
    this.setState({
      details: false,
      form_multitrip_number: 0,
      form_multitrip_search_message: '',
      form_multitrip_vhd_reg_number: '',
      form_multitrip_vhd_id: '',
      form_multitrip_exit_from: '',
      form_multitrip_plbn_out: '',
      form_multitrip_exit_date: '',
      submitVhd: false,
      form_error_message: '',
    });
  }
  searchVhdFromMultitrip(e) {
    e.preventDefault();
    const { form_multitrip_vhd_reg_number, itemsAll } = this.state;

    /*Searching from array*/
    let index = -1;
    for(var i = 0; i < itemsAll.length; i++) {
        if(itemsAll[i]['vechileDetail']['card_number'] === form_multitrip_vhd_reg_number && itemsAll[i]['status'] === 'Ongoing' ) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.setState({
        details: itemsAll[index],
        form_multitrip_number: (itemsAll[index].multitrip.length+1),
        form_multitrip_search_message: '',
        form_multitrip_vhd_id: itemsAll[index].id
      }, () => {
        this.formMultitrip(false)
      })
    } else {
      this.setState({
        details: false,
        form_multitrip_number: 0,
        form_multitrip_search_message: 'Active VhD Not Found.'
      }, () => {
        this.formMultitrip(false)
      })
    }
    /*End of searching on array*/
    return false;
  }
  formMultitripHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({
      [nam]: val
    });
  }
  saveMultitrip(e) {
    e.preventDefault();
    const url = '';
    const formData = new FormData();
    formData.set('vhd_id', this.state.form_multitrip_vhd_id)
    formData.set('exit_from', this.state.form_multitrip_exit_from)
    formData.set('plbn_out', this.state.form_multitrip_plbn_out)
    formData.set('exit_date', this.state.form_multitrip_exit_date)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    this.setState({submitVhd: true}, () => {
      this.formMultitrip(false);
      axios({
        method: 'post',
        url: url,
        data: formData,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then((response) => {
        //handle success
        this.setState({submitVhd: false});
        this.toggleDetail();
        this.resetFormMultitrip();
      })
      .catch((response) => {
        //handle error
        this.setState({submitVhd: false, form_error_message: 'Error: Failed to submit data!'}, () => {
          this.formMultitrip(false)
        });
      });
    });
  }
  formMultitrip(showModal) {
    if ( typeof showModal === 'undefined' )
      showModal = true;
    const messageStyle = {
      fontSize: '14px',
      display: 'block'
    };
    const formView = (
      <>
      <Form onSubmit={this.searchVhdFromMultitrip.bind(this)}>
        {
          this.state.form_error_message === '' 
          ? null
          : (
              <div className="alert alert-danger">{this.state.form_error_message}</div>
            )
        }
        <FormGroup row>
          <Col md="4">
            <Label>VhD Registration Number</Label>
          </Col>
          <Col xs="12" md="8">
            <InputGroup>
              <Input type="text" name="form_multitrip_vhd_reg_number" innerRef={this.state.form_multitrip_vhd_reg_number} defaultValue={this.state.form_multitrip_vhd_reg_number} required onChange={(ev) => this.formMultitripHandler(ev)} />
              <InputGroupAddon addonType="append">
                <Button type="submit" color="secondary"><i className="fa fa-search"></i></Button>
              </InputGroupAddon>
            </InputGroup>
            {
              this.state.form_multitrip_search_message !== '' 
              ? (
                <FormFeedback className="help-block" style={messageStyle}>{this.state.form_multitrip_search_message}</FormFeedback>
              ): null
            }
          </Col>
        </FormGroup>
        {
          this.state.details ? (
            <>
              <FormGroup>
                <div className="alert alert-primary">Valid until {this.state.details.expired}<br/>This will be your #{this.state.form_multitrip_number} trip</div>
              </FormGroup>
              <FormGroup>
                <Label>Owner Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.owner}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseOwnerInformation" onClick={() => { this.toggleFormMultitripOwner() }}>{this.state.collapseOwnerInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseOwnerInformation} id="collapseOwnerInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Full Name as on Driving License</Label>
                        <span className="form-control">{this.state.details.ownerDetail.title} {this.state.details.ownerDetail.name}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Address</Label>
                        <span className="form-control">{this.state.details.ownerDetail.address}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Identity</Label>
                        <span className="form-control">{this.state.details.ownerDetail.identity}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Identity Number</Label>
                        <span className="form-control">{this.state.details.ownerDetail.identityNumber}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Driver Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.driver}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseDriverInformation" onClick={() => { this.toggleFormMultitripDriver() }}>{this.state.collapseDriverInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseDriverInformation} id="collapseDriverInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Full Name as on ID/Passport/Driving License</Label>
                        <span className="form-control">{this.state.details.driverDetail.title} {this.state.details.driverDetail.name}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Address</Label>
                        <span className="form-control">{this.state.details.driverDetail.address}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Driver License</Label>
                        <span className="form-control">{this.state.details.driverDetail.driverLicense}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Vechile Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.plate}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseVechileInformation" onClick={() => { this.toggleFormMultitripVechile() }}>{this.state.collapseVechileInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseVechileInformation} id="collapseVechileInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Vechile Registration Card No.</Label>
                        <span className="form-control">{this.state.details.vechileDetail.card_number}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Registration Plate No.</Label>
                        <span className="form-control">{this.state.details.vechileDetail.plate_number}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Country Of Registrant</Label>
                        <span className="form-control">{this.state.details.vechileDetail.country_of_registrant}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Brand Of Vechile</Label>
                        <span className="form-control">{this.state.details.vechileDetail.brand}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Entrance Information</Label>
                <Card>
                  <CardBody>
                    <div className="form-horizontal">
                      <FormGroup row>
                        <Col md="3">
                          <Label>Enter From</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.enter_from}</span>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label>PLBN In</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.plbn_in}</span>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label>Entrance Date</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.entrance_date}</span>
                        </Col>
                      </FormGroup>
                    </div>
                  </CardBody>
                </Card>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      <Form onSubmit={this.saveMultitrip.bind(this)}>
        {
          this.state.details ? (
            <>
              <FormGroup>
                <Label>Exit Information</Label>
                <Card>
                  <CardBody>
                    <div className="form-horizontal">
                      <FormGroup row>
                        <Col md="4">
                          <Label>Exit From</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="select" name="form_multitrip_exit_from" innerRef={this.state.form_multitrip_exit_from} defaultValue={this.state.form_multitrip_exit_from} onChange={(ev) => this.formMultitripHandler(ev)}>
                            {
                              this.state.country_options.map((item, index) => {
                                const {id, name} = item;
                                return (
                                  <option key={index} value={id}>{name}</option>   
                                );
                              })
                            }
                          </Input>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>PLBN Out</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="select" name="form_multitrip_plbn_out" innerRef={this.state.form_multitrip_plbn_out} defaultValue={this.state.form_multitrip_plbn_out} onChange={(ev) => this.formMultitripHandler(ev)}>
                            {
                              this.state.plbn_options.map((item, index) => {
                                const {id, name} = item;
                                return (
                                  <option key={index} value={id}>{name}</option>   
                                );
                              })
                            }
                          </Input>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>Exit Date</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="date" name="form_multitrip_exit_date" innerRef={this.state.form_multitrip_exit_date} defaultValue={this.state.form_multitrip_exit_date} onChange={(ev) => this.formMultitripHandler(ev)} />
                        </Col>
                      </FormGroup>
                    </div>
                  </CardBody>
                </Card>
              </FormGroup>
              <FormGroup className="text-center">
                <Button color="secondary" onClick={this.toggleDetail}>Cancel</Button>&nbsp;
                <Button type="submit" id="btnSubmit" color="primary" disabled={this.state.submitVhd}>{this.state.submitVhd ? (<><i className="fa fa-spinner fa-spin"></i> Processing</>): 'Submit'}</Button>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      </>
    );
    this.setState({
      modalDetailContent: formView,
      modalDetailTitle: 'Multitrip',
      modalFooter: false
    });
    if ( showModal )
      this.toggleDetail();
  }
  resetFormExtension() {
    this.setState({
      details: false,
      form_extension_search_message: '',
      form_extension_vhd_reg_number: '',
      form_extension_vhd_id: '',
      form_extension_number: 0,
      form_extension_quota: 150,
      form_extension_checked_request: false,
      submitVhd: false,
      form_error_message: ''
    });
  }
  searchVhdFromExtension(e) {
    e.preventDefault();
    const { form_extension_vhd_reg_number, itemsAll } = this.state;

    /*Searching from array*/
    let index = -1;
    for(var i = 0; i < itemsAll.length; i++) {
        if(itemsAll[i]['vechileDetail']['card_number'] === form_extension_vhd_reg_number && itemsAll[i]['status'] === 'Ongoing' ) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.setState({
        details: itemsAll[index],
        form_extension_number: (itemsAll[index].extensions.length+1),
        form_extension_quota: (150-(itemsAll[index].extensions.length*30)),
        form_extension_search_message: '',
        form_extension_vhd_id: itemsAll[index].id
      }, () => {
        this.formExtension(false)
      })
    } else {
      this.setState({
        details: false,
        form_extension_number: 0,
        form_extension_quota: 150,
        form_extension_search_message: 'Active VhD Not Found.'
      }, () => {
        this.formExtension(false)
      })
    }
    /*End of searching on array*/
    return false;
  }
  formExtensionHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    if ( nam === 'form_extension_checked_request' ) {
      this.setState({
        [nam]: !this.state.form_extension_checked_request
      }, () => {
        this.formExtension(false)
      });
    } else {
      this.setState({
        [nam]: val
      });
    }
  }
  saveExtension(e) {
    e.preventDefault();
    const url = '';
    const formData = new FormData();
    formData.set('vhd_id', this.state.form_extension_vhd_id)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    this.setState({submitVhd: true}, () => {
      this.formExtension(false);
      axios({
        method: 'post',
        url: url,
        data: formData,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then((response) => {
        //handle success
        this.setState({submitVhd: false});
        this.toggleDetail();
        this.resetFormExtension();
      })
      .catch((response) => {
        //handle error
        this.setState({submitVhd: false, form_error_message: 'Error: Failed to submit data!'}, () => {
          this.formExtension(false)
        });
      });
    });
  }
  formExtension(showModal) {
    if ( typeof showModal === 'undefined' )
      showModal = true;
    const messageStyle = {
      fontSize: '14px',
      display: 'block'
    };
    const formView = (
      <>
      <Form onSubmit={this.searchVhdFromExtension.bind(this)}>
        {
          this.state.form_error_message === '' 
          ? null
          : (
              <div className="alert alert-danger">{this.state.form_error_message}</div>
            )
        }
        <FormGroup row>
          <Col md="4">
            <Label>VhD Registration Number</Label>
          </Col>
          <Col xs="12" md="8">
            <InputGroup>
              <Input type="text" name="form_extension_vhd_reg_number" innerRef={this.state.form_extension_vhd_reg_number} defaultValue={this.state.form_extension_vhd_reg_number} required onChange={(ev) => this.formExtensionHandler(ev)} />
              <InputGroupAddon addonType="append">
                <Button type="submit" color="secondary"><i className="fa fa-search"></i></Button>
              </InputGroupAddon>
            </InputGroup>
            {
              this.state.form_extension_search_message !== '' 
              ? (
                <FormFeedback className="help-block" style={messageStyle}>{this.state.form_extension_search_message}</FormFeedback>
              ): null
            }
          </Col>
        </FormGroup>
        {
          this.state.details ? (
            <>
              <FormGroup>
                <div className="alert alert-primary">Valid until {this.state.details.expired}<br/>Extension quota : {this.state.form_extension_quota} days<br/>This will be your #{this.state.form_extension_number} extension</div>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      <Form onSubmit={this.saveExtension.bind(this)}>
        {
          this.state.details && this.state.form_extension_quota > 0 ? (
            <>
              <FormGroup>
                <FormGroup check className="checkbox">
                  <Input className="form-check-input" type="checkbox" id="form_extension_checked_request" name="form_extension_checked_request" checked={this.state.form_extension_checked_request} onClick={(ev) => this.formExtensionHandler(ev)} />
                  <Label check className="form-check-label" htmlFor="form_extension_checked_request">Request for extension (30 days)</Label>
                </FormGroup>  
              </FormGroup>
              <FormGroup className="text-center">
                <Button color="secondary" onClick={this.toggleDetail}>Cancel</Button>&nbsp;
                <Button type="submit" id="btnSubmit" color="primary" disabled={this.state.submitVhd || (!this.state.submitVhd && !this.state.form_extension_checked_request) ? true: false}>{this.state.submitVhd ? (<><i className="fa fa-spinner fa-spin"></i> Processing</>): 'Submit'}</Button>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      </>
    );
    this.setState({
      modalDetailContent: formView,
      modalDetailTitle: 'Extension',
      modalFooter: false
    });
    if ( showModal )
      this.toggleDetail();
  }
  resetFormAccomplishment() {
    this.setState({
      details: false,
      form_accomplishment_search_message: '',
      form_accomplishment_vhd_reg_number: '',
      form_accomplishment_vhd_id: '',
      form_accomplishment_type: 'Export Back',
      form_accomplishment_depart_to: '',
      form_accomplishment_gate_out: '',
      form_accomplishment_exit_date: '',
      form_accomplishment_cause: '',
      form_accomplishment_vechile_owner_approval: '',
      form_accomplishment_bc_statement: '',
      form_accomplishment_bnpb_statement: '',
      form_accomplishment_police_statement: '',
      plbn_options: this.state.default_plbn_options,
      submitVhd: false,
      form_error_message: '',
    });
  }
  searchVhdFromAccomplishment(e) {
    e.preventDefault();
    const { form_accomplishment_vhd_reg_number, itemsAll } = this.state;

    /*Searching from array*/
    let index = -1;
    for(var i = 0; i < itemsAll.length; i++) {
        if(itemsAll[i]['vechileDetail']['card_number'] === form_accomplishment_vhd_reg_number && itemsAll[i]['status'] === 'Ongoing' ) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.setState({
        details: itemsAll[index],
        form_accomplishment_search_message: '',
        form_accomplishment_vhd_id: itemsAll[index].id
      }, () => {
        this.formAccomplishment(false)
      })
    } else {
      this.setState({
        details: false,
        form_accomplishment_search_message: 'Active VhD Not Found.'
      }, () => {
        this.formAccomplishment(false)
      })
    }
    /*End of searching on array*/
    return false;
  } 
  formAccomplishmentHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    if ( nam === 'form_accomplishment_vechile_owner_approval' || nam === 'form_accomplishment_bc_statement' || nam === 'form_accomplishment_bnpb_statement' || nam === 'form_accomplishment_police_statement' ) {
      this.setState({[nam]: event.target.files[0]});
    } else {
      this.setState({
        [nam]: val
      }, () => {
        if ( nam === 'form_accomplishment_depart_to' ) {
          let options = [];
          for(var i = 0; i < this.state.plbn_data.length; i++) {
            let item = this.state.plbn_data[i];
            if ( parseInt(item.country) === parseInt(val) ) {
              options.push(item);
            }
          }
          this.setState({plbn_options: options}, () => {
            this.formAccomplishment(false);
          });
        } else if ( nam === 'form_accomplishment_type' ) {
          this.formAccomplishment(false);
        }
      });
    }
  }
  saveAccomplishment(e) {
    e.preventDefault();
    const url = '';
    const formData = new FormData();
    formData.set('vhd_id', this.state.form_accomplishment_vhd_id)
    formData.set('type', this.state.form_accomplishment_type)
    formData.set('depart_to', this.state.form_accomplishment_depart_to)
    formData.set('gate_out', this.state.form_accomplishment_gate_out)
    formData.set('exit_date', this.state.form_accomplishment_exit_date)
    formData.set('cause', this.state.form_accomplishment_cause)
    formData.append('vechile_owner_approval', this.state.form_accomplishment_vechile_owner_approval)
    formData.append('bc_statement', this.state.form_accomplishment_bc_statement)
    formData.append('bnpb_statement', this.state.form_accomplishment_bnpb_statement)
    formData.append('police_statement', this.state.form_accomplishment_police_statement)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    this.setState({submitVhd: true}, () => {
      this.formAccomplishment(false);
      axios({
        method: 'post',
        url: url,
        data: formData,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then((response) => {
        //handle success
        this.setState({submitVhd: false});
        this.toggleDetail();
        this.resetFormAccomplishment();
      })
      .catch((response) => {
        //handle error
        this.setState({submitVhd: false, form_error_message: 'Error: Failed to submit data!'}, () => {
          this.formAccomplishment(false)
        });
      });
    });
  }
  formAccomplishment(showModal) {
    if ( typeof showModal === 'undefined' )
      showModal = true;
    const messageStyle = {
      fontSize: '14px',
      display: 'block'
    };
    const formView = (
      <>
      <Form onSubmit={this.searchVhdFromAccomplishment.bind(this)}>
        {
          this.state.form_error_message === '' 
          ? null
          : (
              <div className="alert alert-danger">{this.state.form_error_message}</div>
            )
        }
        <FormGroup row>
          <Col md="4">
            <Label>VhD Registration Number</Label>
          </Col>
          <Col xs="12" md="8">
            <InputGroup>
              <Input type="text" name="form_accomplishment_vhd_reg_number" innerRef={this.state.form_accomplishment_vhd_reg_number} defaultValue={this.state.form_accomplishment_vhd_reg_number} required onChange={(ev) => this.formAccomplishmentHandler(ev)} />
              <InputGroupAddon addonType="append">
                <Button type="submit" color="secondary"><i className="fa fa-search"></i></Button>
              </InputGroupAddon>
            </InputGroup>
            {
              this.state.form_accomplishment_search_message !== '' 
              ? (
                <FormFeedback className="help-block" style={messageStyle}>{this.state.form_accomplishment_search_message}</FormFeedback>
              ): null
            }
          </Col>
        </FormGroup>
        {
          this.state.details ? (
            <>
              <FormGroup>
                <Label>Owner Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.owner}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseOwnerInformation" onClick={() => { this.toggleFormAccomplishmentOwner() }}>{this.state.collapseOwnerInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseOwnerInformation} id="collapseOwnerInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Full Name as on Driving License</Label>
                        <span className="form-control">{this.state.details.ownerDetail.title} {this.state.details.ownerDetail.name}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Address</Label>
                        <span className="form-control">{this.state.details.ownerDetail.address}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Identity</Label>
                        <span className="form-control">{this.state.details.ownerDetail.identity}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Identity Number</Label>
                        <span className="form-control">{this.state.details.ownerDetail.identityNumber}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Driver Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.driver}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseDriverInformation" onClick={() => { this.toggleFormAccomplishmentDriver() }}>{this.state.collapseDriverInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseDriverInformation} id="collapseDriverInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Full Name as on ID/Passport/Driving License</Label>
                        <span className="form-control">{this.state.details.driverDetail.title} {this.state.details.driverDetail.name}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Address</Label>
                        <span className="form-control">{this.state.details.driverDetail.address}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Driver License</Label>
                        <span className="form-control">{this.state.details.driverDetail.driverLicense}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Vechile Information</Label>
                <Card>
                  <CardHeader>
                    {this.state.details.plate}
                    <div className="card-header-actions">
                      <a className="card-header-action btn btn-minimize link" data-target="#collapseVechileInformation" onClick={() => { this.toggleFormAccomplishmentVechile() }}>{this.state.collapseVechileInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
                    </div>
                  </CardHeader>
                  <Collapse isOpen={this.state.collapseVechileInformation} id="collapseVechileInformation">
                    <CardBody>
                      <FormGroup>
                        <Label>Vechile Registration Card No.</Label>
                        <span className="form-control">{this.state.details.vechileDetail.card_number}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Registration Plate No.</Label>
                        <span className="form-control">{this.state.details.vechileDetail.plate_number}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Country Of Registrant</Label>
                        <span className="form-control">{this.state.details.vechileDetail.country_of_registrant}</span>
                      </FormGroup>
                      <FormGroup>
                        <Label>Brand Of Vechile</Label>
                        <span className="form-control">{this.state.details.vechileDetail.brand}</span>
                      </FormGroup>
                    </CardBody>
                  </Collapse>
                </Card>
              </FormGroup>
              <FormGroup>
                <Label>Entrance Information</Label>
                <Card>
                  <CardBody>
                    <div className="form-horizontal">
                      <FormGroup row>
                        <Col md="3">
                          <Label>Enter From</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.enter_from}</span>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label>PLBN In</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.plbn_in}</span>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label>Entrance Date</Label>
                        </Col>
                        <Col xs="12" md="9">
                          <span className="form-control">{this.state.details.entrance_date}</span>
                        </Col>
                      </FormGroup>
                    </div>
                  </CardBody>
                </Card>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      <Form onSubmit={this.saveAccomplishment.bind(this)}>
        {
          this.state.details ? (
            <>
              <FormGroup>
                <Label>Exit Information</Label>
                <Card>
                  <CardBody>
                    <div className="form-horizontal">
                      <FormGroup row>
                        <Col md="4">
                          <Label>Type</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input name="form_accomplishment_type" type="select" innerRef={this.state.form_accomplishment_type} defaultValue={this.state.form_accomplishment_type} onChange={(ev) => this.formAccomplishmentHandler(ev)}>
                            <option value="Export Back">Export Back</option>
                            <option value="Other">Other</option>
                          </Input>
                        </Col>
                      </FormGroup>
                      {
                        this.state.form_accomplishment_type === 'Export Back'
                        ? (
                          <>
                            <FormGroup row>
                              <Col md="4">
                                <Label>Depart To</Label>
                              </Col>
                              <Col xs="12" md="8">
                                <Input type="select" name="form_accomplishment_depart_to" innerRef={this.state.form_accomplishment_depart_to} defaultValue={this.state.form_accomplishment_depart_to} onChange={(ev) => this.formAccomplishmentHandler(ev)}>
                                  {
                                    this.state.country_options.map((item, index) => {
                                      const {id, name} = item;
                                      return (
                                        <option key={index} value={id}>{name}</option>   
                                      );
                                    })
                                  }
                                </Input>
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Col md="4">
                                <Label>Gate Out</Label>
                              </Col>
                              <Col xs="12" md="8">
                                <Input type="select" name="form_accomplishment_gate_out" innerRef={this.state.form_accomplishment_gate_out} defaultValue={this.state.form_accomplishment_gate_out} onChange={(ev) => this.formAccomplishmentHandler(ev)}>
                                  {
                                    this.state.plbn_options.map((item, index) => {
                                      const {id, name} = item;
                                      return (
                                        <option key={index} value={id}>{name}</option>   
                                      );
                                    })
                                  }
                                </Input>
                              </Col>
                            </FormGroup>
                          </>
                        ) : null
                      }
                      <FormGroup row>
                        <Col md="4">
                          <Label>Exit Date</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="date" name="form_accomplishment_exit_date" innerRef={this.state.form_accomplishment_exit_date} defaultValue={this.state.form_accomplishment_exit_date} required onChange={(ev) => this.formAccomplishmentHandler(ev)} />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>Cause</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input name="form_accomplishment_cause" type="select" innerRef={this.state.form_accomplishment_cause} defaultValue={this.state.form_accomplishment_cause} required onChange={(ev) => this.formAccomplishmentHandler(ev)}>
                            <option value="Broken">Broken</option>
                            <option value="Lost">Lost</option>
                            <option value="Force Majeur">Force Majeur</option>
                            <option value="Other">Other</option>
                          </Input>
                        </Col>
                      </FormGroup>
                      <FormGroup>
                        <Label>Proof Documents</Label>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>Vechile Owner Approval</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="file" name="form_accomplishment_vechile_owner_approval" onChange={(ev) => this.formVhdHandler(ev)} />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>BC Statement Of Goods License</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="file" name="form_accomplishment_bc_statement" onChange={(ev) => this.formVhdHandler(ev)} />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>BNPB Statement Of Force Majeur</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="file" name="form_accomplishment_bnpb_statement" onChange={(ev) => this.formVhdHandler(ev)} />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="4">
                          <Label>Police Statement</Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Input type="file" name="form_accomplishment_police_statement" onChange={(ev) => this.formVhdHandler(ev)} />
                        </Col>
                      </FormGroup>
                    </div>
                  </CardBody>
                </Card>
              </FormGroup>
              <FormGroup className="text-center">
                <Button color="secondary" onClick={this.toggleDetail}>Cancel</Button>&nbsp;
                <Button type="submit" id="btnSubmit" color="primary" disabled={this.state.submitVhd}>{this.state.submitVhd ? (<><i className="fa fa-spinner fa-spin"></i> Processing</>): 'Submit'}</Button>
              </FormGroup>
            </>
          ): null
        }
      </Form>
      </>
    );
    this.setState({
      modalDetailContent: formView,
      modalDetailTitle: 'Accomplishment',
      modalFooter: false
    });
    if ( showModal )
      this.toggleDetail();
  }
  viewTrackingProcess(index, list) {
    const dataDetail = this.state[list][index];
    const trackings = dataDetail.verification_process;
    let count_data = trackings.length;
    let count_separator = count_data - 1;
    let top_row = [];
    let bottom_row = [];
    let middle_row = [];
    let desc_row = [];

    const topRow = () => {
      for (let i = 0; i < count_data; i++) {
        if ( i < (count_data-1) ) {
          top_row.push(
            <>
            <td rowSpan="3" className="text-center" width="100px"><h1><span className="fa fa-user"></span></h1></td>
            <td height="17px"></td>
            </>
          );
        } else {
          top_row.push(
            <>
            <td rowSpan="3" className="text-center" width="100px"><h1><span className="fa fa-user"></span></h1></td>
            </>
          );
        }
      }
      return top_row;
    };
    const bottomRow = () => {
      for (let i = 0; i < count_separator; i++) {
        bottom_row.push(<td height="17px"></td>);
      }
      return bottom_row;
    };
    const middleRow = () => {
      for (let i = 0; i < count_separator; i++) {
        middle_row.push(<td height="1px" width="80px" bgColor="#000000"></td>);
      }
      return middle_row;
    };
    const descRow = () => {
      let num = 1;
      trackings.map((item, index) => {
        const {process} = item;
        if ( num < count_data ) {
          desc_row.push(
            <>
            <td className="text-center">{process}</td>
            <td></td>
            </>
          );
        } else {
          desc_row.push(
            <>
            <td className="text-center">{process}</td>
            </>
          );
        }
        num++;
      });
      return desc_row;
    };
    const detailView = (
      <table>
        <tbody>
          <tr>{topRow()}</tr>
          <tr>{middleRow()}</tr>
          <tr>{bottomRow()}</tr>
          <tr>{descRow()}</tr>
        </tbody>
      </table>
    );
    this.setState({
      modalDetailContent: detailView,
      modalDetailTitle: 'Tracking VhD',
      modalFooter: true
    });
    this.toggleDetail();
  }
  viewDetail(index, list, showModal) {
    if ( typeof showModal === 'undefined' )
      showModal = true;
    const dataDetail = this.state[list][index];
    const detailView = (
      <>
        <Nav tabs>
          <NavItem>
            <NavLink
              active={this.state.activeDetailTab[0] === '1'}
              onClick={() => { this.toggleDetailTabs(0, '1', index, list); }}>
              VhD Detail
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={this.state.activeDetailTab[0] === '2'}
              onClick={() => { this.toggleDetailTabs(0, '2', index, list); }}>
              Multitrip
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={this.state.activeDetailTab[0] === '3'}
              onClick={() => { this.toggleDetailTabs(0, '3', index, list); }}>
              Extension
            </NavLink>
          </NavItem>
          {
            dataDetail.status.toLowerCase() === 'done'
            ? (
                <NavItem>
                  <NavLink
                    active={this.state.activeDetailTab[0] === '4'}
                    onClick={() => { this.toggleDetailTabs(0, '4', index, list); }}>
                    Accomplishment
                  </NavLink>
                </NavItem>
              )
            : null
          }
        </Nav>
        <TabContent activeTab={this.state.activeDetailTab[0]}>
          {this.tabDetailPane(index, list, dataDetail)}
        </TabContent>
      </>
    );
    this.setState({
      modalDetailContent: detailView,
      modalDetailTitle: 'VhD Detail',
      modalFooter: true
    });
    if ( showModal )
      this.toggleDetail();
  }

  vhdDetail(index, list, dataDetail) {
    const ownerDetail = dataDetail.ownerDetail;
    const driverDetail = dataDetail.driverDetail;
    const vechileDetail = dataDetail.vechileDetail;
    return (
      <div className="form">
        <div className="form-group">
          <table width="100%">
            <tbody>
              <tr>
                <td colSpan="3">VhD Registration No {dataDetail.aju}</td>
                <td className="text-right">{dataDetail.regDate}</td>
              </tr>
              <tr>
                <td rowSpan="2" width="50px"><h1><i className="fa fa-car"></i></h1></td>
                <td rowSpan="2" width="10px"></td>
                <td>{dataDetail.plate}</td>
                <td rowSpan="2" className="text-right">{dataDetail.status}</td>
              </tr>
              <tr>
                <td>Valid Until {dataDetail.expired === '' || dataDetail.expired === null ? '-': dataDetail.expired}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <FormGroup>
          <Label>Owner Information</Label>
          <Card>
            <CardHeader>
              {dataDetail.owner}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseOwnerInformation" onClick={() => { this.toggleOwnerInformation(index, list) }}>{this.state.collapseOwnerInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseOwnerInformation} id="collapseOwnerInformation">
              <CardBody>
                <FormGroup>
                  <Label>Full Name as on Driving License</Label>
                  <span className="form-control">{ownerDetail.title} {ownerDetail.name}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Address</Label>
                  <span className="form-control">{ownerDetail.address}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Identity</Label>
                  <span className="form-control">{ownerDetail.identity}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Identity Number</Label>
                  <span className="form-control">{ownerDetail.identityNumber}</span>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
        <FormGroup>
          <Label>Driver Information</Label>
          <Card>
            <CardHeader>
              {dataDetail.driver}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseDriverInformation" onClick={() => { this.toggleDriverInformation(index, list) }}>{this.state.collapseDriverInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseDriverInformation} id="collapseDriverInformation">
              <CardBody>
                <FormGroup>
                  <Label>Full Name as on ID/Passport/Driving License</Label>
                  <span className="form-control">{driverDetail.title} {driverDetail.name}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Address</Label>
                  <span className="form-control">{driverDetail.address}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Driver License</Label>
                  <span className="form-control">{driverDetail.driverLicense}</span>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
        <FormGroup>
          <Label>Vechile Information</Label>
          <Card>
            <CardHeader>
              {dataDetail.plate}
              <div className="card-header-actions">
                <a className="card-header-action btn btn-minimize link" data-target="#collapseVechileInformation" onClick={() => { this.toggleVechileInformation(index, list) }}>{this.state.collapseVechileInformation ? <i className="icon-arrow-up"></i>: <i className="icon-arrow-right"></i>}</a>
              </div>
            </CardHeader>
            <Collapse isOpen={this.state.collapseVechileInformation} id="collapseVechileInformation">
              <CardBody>
                <FormGroup>
                  <Label>Vechile Registration Card No.</Label>
                  <span className="form-control">{vechileDetail.card_number}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Registration Plate No.</Label>
                  <span className="form-control">{vechileDetail.plate_number}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Country Of Registrant</Label>
                  <span className="form-control">{vechileDetail.country_of_registrant}</span>
                </FormGroup>
                <FormGroup>
                  <Label>Brand Of Vechile</Label>
                  <span className="form-control">{vechileDetail.brand}</span>
                </FormGroup>
              </CardBody>
            </Collapse>
          </Card>
        </FormGroup>
        <FormGroup>
          <Label>Entrance Information</Label>
          <Card>
            <CardBody>
              <div className="form-horizontal">
                <FormGroup row>
                  <Col md="3">
                    <Label>Enter From</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{dataDetail.enter_from}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>PLBN In</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{dataDetail.plbn_in}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Entrance Date</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{dataDetail.entrance_date}</span>
                  </Col>
                </FormGroup>
              </div>
            </CardBody>
          </Card>
        </FormGroup>
      </div>
    )
  }

  multitripDetail(index, list, dataDetail) {
    const multitripData = dataDetail.multitrip;
    return (
      <Table responsive>
        <thead>
          <tr>
            <th className="text-center">Multitrip<br/>Number</th>
            <th className="text-center">Vechile<br/>Date Arrival</th>
            <th className="text-center">Province Arrival</th>
            <th className="text-center">PLBN Arrival</th>
            <th className="text-center">Vechile<br/>Date Departure</th>
            <th className="text-center">Province Departure</th>
            <th className="text-center">PLBN Departure</th>
          </tr>
        </thead>
        <tbody>
        {
          multitripData.length > 0 ? multitripData.map((item, i) => {
            const {id, number, date_arrival, province_arrival, plbn_arrival, date_departure, province_departure, plbn_departure} = item;
            return (
              <tr key={i}>
                <td>{number}</td>
                <td>{date_arrival}</td>
                <td>{province_arrival}</td>
                <td>{plbn_arrival}</td>
                <td>{date_departure}</td>
                <td>{province_departure}</td>
                <td>{plbn_departure}</td>
              </tr>
              );
            }) : <tr><td colSpan="7" className="text-center">Multitrip is not available</td></tr>
        }
        </tbody>
      </Table>
    )
  }

  extensionDetail(index, list, dataDetail) {
    const extensionsData = dataDetail.extensions;
    return (
      <Table responsive>
        <thead>
          <tr>
            <th className="text-center">No.</th>
            <th className="text-center">Valid Until</th>
            <th className="text-center">Extension Valid Until</th>
            <th className="text-center">Remaining Quota</th>
          </tr>
        </thead>
        <tbody>
        {
          extensionsData.length > 0 ? extensionsData.map((item, i) => {
            const {id, number, valid_until, extension_valid_until, remaining_quota} = item;
            return (
              <tr key={i}>
                <td>{number}</td>
                <td>{valid_until}</td>
                <td>{extension_valid_until}</td>
                <td>{remaining_quota}</td>
              </tr>
              );
            }) : <tr><td colSpan="4" className="text-center">Extension is not available</td></tr>
        }
        </tbody>
      </Table>
    )
  }

  accomplishmentDetail(index, list, dataDetail) {
    const accomplishmentData = dataDetail.accomplishment;
    return (
      <div className="form">
        <FormGroup>
          <Label>Exit Information</Label>
          <Card>
            <CardBody>
              <div className="form-horizontal">
                <FormGroup row>
                  <Col md="3">
                    <Label>Type of Accomp.</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{accomplishmentData.type}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Depart To</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{accomplishmentData.depart_to}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Gate Out</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{accomplishmentData.gate_out}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Exit Date</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{accomplishmentData.exit_date}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Cause</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">{accomplishmentData.cause}</span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="12">
                    <Label>Proof Documents</Label>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Vechile Owner Approval</Label>
                  </Col>
                  <Col xs="12" md="9">
                  <span className="form-control">
                    {
                      accomplishmentData.vechile_owner_approval_doc === ''
                      ? null
                      : (
                        <>{accomplishmentData.vechile_owner_approval_doc} <i className="fa fa-paperclip"></i></>
                      )
                    }
                    </span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>BC Statement Of Good License</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">
                    {
                      accomplishmentData.bc_statement_of_good_licence === ''
                      ? null
                      : (
                        <>{accomplishmentData.bc_statement_of_good_licence} <i className="fa fa-paperclip"></i></>
                      )
                    }
                    </span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>BNPB Statement Of Force Majeur</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">
                    {
                      accomplishmentData.bnpb_statement_of_force_majeur === ''
                      ? null
                      : (
                        <>{accomplishmentData.bnpb_statement_of_force_majeur} <i className="fa fa-paperclip"></i></>
                      )
                    }
                    </span>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label>Police Statement Of Lost</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <span className="form-control">
                    {
                      accomplishmentData.police_statement_of_lost === ''
                      ? null
                      : (
                        <>{accomplishmentData.police_statement_of_lost} <i className="fa fa-paperclip"></i></>
                      )
                    }
                    </span>
                  </Col>
                </FormGroup>
              </div>
            </CardBody>
          </Card>
        </FormGroup>
      </div>
    )
  }

  toggleDetailTabs(tabPane, tab, index, list) {
    const newArray = this.state.activeDetailTab.slice()
    newArray[tabPane] = tab
    this.setState({activeDetailTab: newArray}, () => {
      this.viewDetail(index, list, false);
    });
  }

  tabDetailPane(index, list, data) {
    return (
      <>
        <TabPane tabId="1">
          {this.vhdDetail(index, list, data)}
        </TabPane>
        <TabPane tabId="2">
          {this.multitripDetail(index, list, data)}
        </TabPane>
        <TabPane tabId="3">
          {this.extensionDetail(index, list, data)}
        </TabPane>
        <TabPane tabId="4">
          {this.accomplishmentDetail(index, list, data)}
        </TabPane>
      </>
    ); 
  }

  eventDetailDatatable = (e, list) => {
    let id = e.target.id;
    if ( id === '' ) 
      id = e.target.parentNode.id;
    const items = this.state[list];
    let index = -1;
    for(var i = 0; i < items.length; i++) {
        if(items[i]['aju'] === id) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.viewDetail(index, list);
    }
  }
  trackingProcess = (e, list) => {
    let id = e.target.id;
    if ( id === '' ) 
      id = e.target.parentNode.id;
    const items = this.state[list];
    let index = -1;
    for(var i = 0; i < items.length; i++) {
        if(items[i]['aju'] === id) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.viewTrackingProcess(index, list);
    }
  }

  eventDatatableItemsAll = e => {
    let id = e.target.id;
    if ( id === '' ) 
      id = e.target.parentNode.id;
    const { itemsAll } = this.state;
    let index = -1;
    for(var i = 0; i < itemsAll.length; i++) {
        if(itemsAll[i]['aju'] === id) {
            index = i;
        }
    }
    if ( index !== -1 ) {
      this.viewDetail(index, 'itemsAll');
    }
  }

  allItems() {
    const { loading_itemsAll, itemsAll, total_itemsAll } = this.state;
    
    const datatableColumns = memoize(rowHandler => columns.concat([
      {
        cell: row => (
          row.status === 'Draft' 
          ? null
          : (
            <Button variant="contained" onClick={(row) => rowHandler.tracking(row, 'itemsAll')} id={row.aju} title="Tracking Process"><i className="fa fa-exchange"></i> </Button>
          ) 
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      },
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.detail(row, 'itemsAll')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      }
    ]));
    const dataHandler = {
      detail: this.eventDetailDatatable,
      tracking: this.trackingProcess
    };

    return (
      <DataTable
        title="Temporary Importation - All Processes"
        columns={datatableColumns(dataHandler)}
        data={itemsAll}
        progressPending={loading_itemsAll}
        pagination
        paginationServer
        paginationTotalRows={total_itemsAll}
        onChangeRowsPerPage={this.handlePerRowsItemsAllChange}
        onChangePage={this.handlePageItemsAllChange}/>
    );
  }
  inProcess() {
    const { loading_itemsInProcess, itemsInProcess, total_itemsInProcess } = this.state;
    
    const datatableColumns = memoize(rowHandler => columns.concat([
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.tracking(row, 'itemsInProcess')} id={row.aju} title="Tracking Process"><i className="fa fa-exchange"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      },
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.detail(row, 'itemsInProcess')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      }
    ]));
    const dataHandler = {
      detail: this.eventDetailDatatable,
      tracking: this.trackingProcess
    };
    return (
      <DataTable
        title="Temporary Importation - In Process"
        columns={datatableColumns(dataHandler)}
        data={itemsInProcess}
        progressPending={loading_itemsInProcess}
        pagination
        paginationServer
        paginationTotalRows={total_itemsInProcess}
        onChangeRowsPerPage={this.handlePerRowsItemsInProcessChange}
        onChangePage={this.handlePageItemsInProcessChange}/>
    );
  }
  onGoing() {
    const { loading_itemsOngoing, itemsOngoing, total_itemsOngoing } = this.state;
    
    const datatableColumns = memoize(rowHandler => columns.concat([
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.tracking(row, 'itemsOngoing')} id={row.aju} title="Tracking Process"><i className="fa fa-exchange"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      },
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.detail(row, 'itemsOngoing')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      }
    ]));
    const dataHandler = {
      detail: this.eventDetailDatatable,
      tracking: this.trackingProcess
    };

    return (
      <DataTable
        title="Temporary Importation - Ongoing"
        columns={datatableColumns(dataHandler)}
        data={itemsOngoing}
        progressPending={loading_itemsOngoing}
        pagination
        paginationServer
        paginationTotalRows={total_itemsOngoing}
        onChangeRowsPerPage={this.handlePerRowsItemsOngoingChange}
        onChangePage={this.handlePageItemsOngoingChange}/>
    );
  }
  done() {
    const { loading_itemsDone, itemsDone, total_itemsDone } = this.state;
    
    const datatableColumns = memoize(rowHandler => columns.concat([
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.tracking(row, 'itemsDone')} id={row.aju} title="Tracking Process"><i className="fa fa-exchange"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      },
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.detail(row, 'itemsDone')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      }
    ]));
    const dataHandler = {
      detail: this.eventDetailDatatable,
      tracking: this.trackingProcess
    };

    return (
      <DataTable
        title="Temporary Importation - Done"
        columns={datatableColumns(dataHandler)}
        data={itemsDone}
        progressPending={loading_itemsDone}
        pagination
        paginationServer
        paginationTotalRows={total_itemsDone}
        onChangeRowsPerPage={this.handlePerRowsItemsDoneChange}
        onChangePage={this.handlePageItemsDoneChange}/>
    );
  }
  draft() {
    const { loading_itemsDraft, itemsDraft, total_itemsDraft } = this.state;
    
    const datatableColumns = memoize(detilHandler => columns.concat([
      {
        cell: row => <Button variant="contained" onClick={(row) => detilHandler(row, 'itemsDraft')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      }
    ]));

    return (
      <DataTable
        title="Temporary Importation - Draft"
        columns={datatableColumns(this.eventDetailDatatable)}
        data={itemsDraft}
        progressPending={loading_itemsDraft}
        pagination
        paginationServer
        paginationTotalRows={total_itemsDraft}
        onChangeRowsPerPage={this.handlePerRowsItemsDraftChange}
        onChangePage={this.handlePageItemsDraftChange}/>
    );
  }
  rejected() {
    const { loading_itemsRejected, itemsRejected, total_itemsRejected } = this.state;
    
    const datatableColumns = memoize(rowHandler => columns.concat([
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.tracking(row, 'itemsRejected')} id={row.aju} title="Tracking Process"><i className="fa fa-exchange"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      },
      {
        cell: row => <Button variant="contained" onClick={(row) => rowHandler.detail(row, 'itemsRejected')} id={row.aju} title="View Details"><i className="fa fa-file-text-o"></i> </Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
        width: '35px',
      }
    ]));
    const dataHandler = {
      detail: this.eventDetailDatatable,
      tracking: this.trackingProcess
    };

    return (
      <DataTable
        title="Temporary Importation - Rejected"
        columns={datatableColumns(dataHandler)}
        data={itemsRejected}
        progressPending={loading_itemsRejected}
        pagination
        paginationServer
        paginationTotalRows={total_itemsRejected}
        onChangeRowsPerPage={this.handlePerRowsItemsRejectedChange}
        onChangePage={this.handlePageItemsRejectedChange}/>
    );
  }

  toggle(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }

  tabPane() {
    return (
      <>
        <TabPane tabId="1">
          {this.allItems()}
        </TabPane>
        <TabPane tabId="2">
          {this.inProcess()}
        </TabPane>
        <TabPane tabId="3">
          {this.onGoing()}
        </TabPane>
        <TabPane tabId="4">
          {this.done()}
        </TabPane>
        <TabPane tabId="5">
          {this.draft()}
        </TabPane>
        <TabPane tabId="6">
          {this.rejected()}
        </TabPane>
      </>
    );
  }
  toggleDropdownMenu(i) {
    const newArray = this.state.dropdownMenuOpen.map((element, index) => { return (index === i ? !element : false); });
    this.setState({
      dropdownMenuOpen: newArray,
    });
  }
  renderModal() {
    return this.state.modalDetailContent;
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <div>
              <ButtonDropdown className="mr-1" isOpen={this.state.dropdownMenuOpen[0]} toggle={() => { this.toggleDropdownMenu(0); }}>
                <Button id="caret" color="primary">Add Item</Button>
                <DropdownToggle caret color="primary" />
                <DropdownMenu>
                  <DropdownItem onClick={() => this.formVhD()}>Create VhD</DropdownItem>
                  <DropdownItem onClick={() => this.formMultitrip()}>Multitrip</DropdownItem>
                  <DropdownItem onClick={() => this.formExtension()}>Extend VhD</DropdownItem>
                  <DropdownItem onClick={() => this.formAccomplishment()}>Accomplishment</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
            <hr />
            <Nav tabs>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '1'}
                  onClick={() => { this.toggle(0, '1'); }}
                >
                  All
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '2'}
                  onClick={() => { this.toggle(0, '2'); }}
                >
                  In Process
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '3'}
                  onClick={() => { this.toggle(0, '3'); }}
                >
                  Ongoing
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '4'}
                  onClick={() => { this.toggle(0, '4'); }}
                >
                  Done
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '5'}
                  onClick={() => { this.toggle(0, '5'); }}
                >
                  Draft
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '6'}
                  onClick={() => { this.toggle(0, '6'); }}
                >
                  Rejected
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab[0]}>
              {this.tabPane()}
            </TabContent>
          </Col>
        </Row>
        <Modal isOpen={this.state.modalDetail} toggle={this.toggleDetail} className="detailVhD" size="lg" zIndex="10000">
          <ModalHeader toggle={this.toggleDetail}>{this.state.modalDetailTitle}</ModalHeader>
          <ModalBody>
            {this.renderModal()}  
          </ModalBody>
          {
            this.state.modalFooter
            ? (
              <ModalFooter>
                {this.state.modalDetailButton}
                <Button color="secondary" onClick={this.toggleDetail}>Close</Button>
              </ModalFooter>
            )
            : null
          }
        </Modal>
        <Modal isOpen={this.state.subModal} toggle={this.toggleSubModal.bind(this)} className="subModal" size="md" zIndex="10001" backdrop="static">
          <ModalBody>
            {this.state.subModalContent}  
          </ModalBody>
          <ModalFooter>
            {this.state.subModalButton}
          </ModalFooter>
        </Modal>
      </div>

    );
  }
}

export default Import;

export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Temporary Importation',
      url: '/import',
      icon: 'fa fa-car'
    },
    {
      name: 'Temporary Exportation',
      url: '/export',
      icon: 'fa fa-car'
    }
  ]
};

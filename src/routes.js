import React from 'react';
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Import = React.lazy(() => import('./views/Import'));
const Export = React.lazy(() => import('./views/Export'));
/*const ImporPemilik = React.lazy(() => import('./views/impor/Pemilik'));
const ImporPengemudi = React.lazy(() => import('./views/impor/Pengemudi'));
const ImporKendaraan = React.lazy(() => import('./views/impor/Kendaraan'));*/

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/import', exact: true, name: 'Import', component: Import },
  { path: '/export', exact: true, name: 'Export', component: Export },
  /*{ path: '/impor/pemilik', exact: true, name: 'Data Pemilik', component: ImporPemilik },
  { path: '/impor/pengemudi', exact: true, name: 'Data Pengemudi', component: ImporPengemudi },
  { path: '/impor/kendaraan', exact: true, name: 'Data Kendaraan', component: ImporKendaraan }*/
];

export default routes;
